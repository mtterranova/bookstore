import React, { useContext } from "react";
import { AppBar, Toolbar, Button } from "@mui/material";
import { GlobalContext } from "./GlobalContext";
import AuthModal from "./AuthModal";

const Header = () => {
    const { isAuthenticated, setIsAuthenticated, username, setUsername } = useContext(GlobalContext);

    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <div className="toolbar-contents">
                        <h1>Bookstore</h1>
                        <div className="toolbar-auth-btns">
                            {
                                !isAuthenticated ? (
                                    <>
                                        <AuthModal type="login"/>
                                        <AuthModal type="register"/>
                                    </>
                                ) : (
                                    <>
                                        <Button
                                            type="button"
                                            className="auth-btn"
                                            style={{
                                                border: "1px solid #62aefa",
                                                borderRadius: "6px",
                                                cursor: "pointer",
                                                width: "60px",
                                                padding: "10px",
                                                marginRight: "10px"
                                            }}
                                            onClick={ () => { 
                                                localStorage.clear();
                                                setUsername("");
                                                setIsAuthenticated(false);
                                            }}>
                                                Logout
                                        </Button>
                                        <div>{ username }</div>
                                    </>
                                )
                            }
                        </div>
                    </div>
                </Toolbar>
            </AppBar>
        </>
    )
}

export default Header;