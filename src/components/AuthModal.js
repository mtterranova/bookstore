import React, { useState, useContext } from 'react';
import { Button, Modal, TextField } from '@mui/material';
import axios from 'axios';
import * as Yup from 'yup';
import { GlobalContext } from './GlobalContext';

const AuthModal = (props) => {
  const { setIsAuthenticated, setJwtToken, setUsername } = useContext(GlobalContext);
  const [ open, setOpen ] = useState(false);
  const [ userField, setUserField ] = useState('');
  const [ password, setPassword ] = useState('');
  const [ errors, setErrors ] = useState({});
  const modalType = props.type;

  const handleOpen = () => setOpen(true);

  const handleClose = () => {
    setOpen(false);
    setUserField('');
    setPassword('');
    setErrors({});
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const validationSchema = Yup.object().shape({
      userField: Yup.string().required(),
      password: Yup.string().required(),
    });
    validationSchema
      .validate({ userField, password }, { abortEarly: false })
      .then(() => {
        let route = modalType === "register" ? "register" : "login"
        console.log("process.env");
        console.log(process.env);
        axios.post(`${process.env.REACT_APP_API_DOMAIN}/${route}`, { username: userField, password })
        .then(res => {
          setJwtToken(res.data.token);
          setUsername(res.data.username)
          localStorage.setItem("token", res.data.token);
          localStorage.setItem("username", res.data.username);
          setIsAuthenticated(true);
          handleClose();
        })
        .catch(err => {
          setErrors({ userField: err?.response?.data?.error });
        })
      })
      .catch((err) => {
        const validationErrors = {};
        err.inner.forEach((error) => {
          console.log(error.path)
          console.log(error.message)
          validationErrors[error.path] = error.message;
        });
        setErrors(validationErrors);
      });
  };

  return (
    <>
      <Button className={`${modalType === "register" ? "auth-btn register-btn" : "auth-btn"}`} onClick={handleOpen}>
        {modalType === "register" ? "Register" : "Login"}
      </Button>
      <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <div className="modal-body">
            <h3>{modalType === "register" ? "Registration" : "Login"}</h3>
            <form onSubmit={handleSubmit}>
              <div style={{ marginBottom: "10px" }}>
                <TextField
                  label="Username"
                  variant="outlined"
                  fullWidth
                  value={userField}
                  onChange={e => setUserField(e.target.value)}
                  error={Boolean(errors.userField)}
                  helperText={errors.userField}
                />
              </div>
              <div style={{ marginBottom: "10px" }}>
                <TextField
                  label="Password"
                  variant="outlined"
                  type="password"
                  fullWidth
                  value={password}
                  onChange={e => setPassword(e.target.value)}
                  error={Boolean(errors.password)}
                  helperText={errors.password}
                  style={{ marginBottom: "10px" }}
                />
              </div>
              <Button type="submit" variant="contained" color="primary">
                {modalType === "register" ? "Register" : "Login"}
              </Button>
            </form>
          </div>
        </Modal>
      </div>
    </>
  );
};

export default AuthModal;
