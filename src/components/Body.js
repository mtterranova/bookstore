import React from 'react';

const Body = ({ children }) => {
    return (
        <div style={{ marginTop: "90px", padding: "20px" }}>
            { children }
        </div>
    )
}

export default Body;