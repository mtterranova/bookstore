import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './views/Home';
import Listing from './views/Listing';
import Header from './components/Header';
import Body from './components/Body';
import { useState, useEffect } from "react";
import axios from 'axios';
import { GlobalContext } from './components/GlobalContext';

const App = () => {
  const [ isAuthenticated, setIsAuthenticated ] = useState(false);
  const [ isAdmin, setIsAdmin ] = useState(false);
  const [ jwtToken, setJwtToken ] = useState(localStorage.getItem('token') || null);
  const [ username, setUsername ] = useState(localStorage.getItem('username') || "");
  
  useEffect(() => {
    if (jwtToken) {
      axios.defaults.headers.common['Authorization'] = `Bearer ${jwtToken}`;
      axios.get(`${process.env.REACT_APP_API_DOMAIN}/verify`)
        .then(res => {
          setIsAuthenticated(true);
        })
        .catch(err => {
          console.log({ error: err })
        })
    }
  }, [jwtToken])

  return (
    <GlobalContext.Provider value={{ isAuthenticated, setIsAuthenticated, jwtToken, setJwtToken, isAdmin, setIsAdmin, username, setUsername }}>
      <Header />
      <Body>
        <Router>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/listing" element={<Listing />} />
          </Routes>
        </Router>
      </Body>
    </GlobalContext.Provider>
  );
};

export default App;