import React from 'react';

const Listing = () => {
  return (
    <div>
      <h1>Listing</h1>
      <p>Welcome to the listing page!</p>
    </div>
  );
};

export default Listing;